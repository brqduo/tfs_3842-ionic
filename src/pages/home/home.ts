import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { QuadradoService } from '../../services/quadraros.service'
import { HttpClient } from '@angular/common/http';
//import { RecebimentoPage } from '../../Applications/Recebimento_Fisico/pages/recebimento/recebimento';
import { LoginService, SecurityService } from '@ons/ons-mobile-login';
import { Config } from '../../enviroment/config';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  items = [];
  posicao = 0;
  totalPagina = 2;
  classe = '';
  data: any = [];
  maxHeight = 90;
  cslasseColuna = 'w-100';
  // OpcoesDisponiveis = 4
  confirma: any = {};
  Tela_Saida_Visivel = false;
  IsAllowed = false;
  NoAppAllowed = Config.mensagemGenerica.SEM_APPS_PERMITDOS;

  constructor(public navCtrl: NavController
    , public quadradoSrv: QuadradoService
    , private loginSrv: LoginService
    , private alertController: AlertController
    , private securitySrv: SecurityService
    , public http: HttpClient) {
 
    }

  ionViewDidLoad() {
    console.log('loaded');
    if (this.quadradoSrv.rangeValid(this.quadradoSrv.qtdItems)) {
      this.loadData();
    } else {
      alert('QTD de items invalida')
      this.navCtrl.pop()
    }
    console.log('Data DidLoad: ', this.data);

  }

  /**
   * 
   * goToApp
   * 
   * Abre aplicativo
   * 
   * @param item Objeto Menu
   */
  goToApp(item) {
    if(item)
    {
          this.navCtrl.setRoot(item.AppName)
    }
  }

  /**
   * 
   * getSomeClass
   * 
   * Recupera Classe
   * 
   * @param item Objeto Menu
   */
  getSomeClass(item: any) {
    const ordem = item == undefined ? '' : item.ordem;
    return this.quadradoSrv.classeQuadradoPerfeito(ordem, this.data.length);
  }

  /**
   * 
   * getMyStyles
   * 
   * Recupera caracteristicas do Objeto Menu.
   * 
   * @param item Objeto Menu
   */
  getMyStyles(item) {
    if(item)
    {
      let tam = this.SetHeigth(this.data.length);
      let myStyles = {
        'background-color': item.cor,
        'height': tam
      };
      return myStyles;
    }
  }

  /**
   * 
   * SetHeigth
   * 
   * Define tamanho da página conforme a quantidade de apps.
   * 
   * @param tamanho Size
   * 
   */
  SetHeigth(tamanho): string {
    var result = '';
    switch (tamanho) {
      case (tamanho >= 2 && tamanho <= 4): {
        result = '50'
        break;
      }
      case (tamanho === 7 || tamanho === 8 || tamanho === 10 || tamanho === 11 || tamanho === 12): {
        result = '22.5'
        break;
      }
      case (tamanho === 6 || tamanho === 5 || tamanho === 9): {
        result = '30'
        break;
      }
      case (tamanho >= 13): {
        result = '18'
        break;
      }
    }
    return result + 'vh'
  }

/**
 * getClass
 * 
 * Recupera Classes
 */
  getClass() {
    console.log('passei pro getClass');

    this.classe = this.quadradoSrv.classeQuadradoPerfeito(this.posicao, this.totalPagina)
  }

  /**
   * 
   * getJSON
   * 
   * Recupera menu.json com os apps cadastrados.
   * 
   */
  private getJSON() {
    return this.http.get("./assets/data/menu.json")
      .subscribe((data: any) => {
        //console.log('fff', data);
        var AppsAllowed: any = [];

        if(data != undefined || data != null)
        {
          for (let index = 0; index < data.length; index++) {
            const menu = data[index];
            
            if(menu.Operations)
            {
                for (let index = 0; index < menu.Operations.length; index++) {
                  const operation = menu.Operations[index];
                  
                  console.log('Type: ONS, Operation: ', operation);
                  
                  
                  
                  if(this.securitySrv.isOperationAllowedByScope('ONS','ONS',operation))
                  {
                    AppsAllowed.push(menu);
                    console.log(operation);
                  }

                }
            }
            
          }
        }

        // data.forEach(element => {
        //   if(element.Operations)
        //   {

        //   }
        // });
        
        this.data = data;
        this.data = AppsAllowed;
        this.quadradoSrv.updateAppsAllowed(this.data.length);
        console.log('Data:', this.data.length);
        console.log('Data Apps Allowed:', AppsAllowed);
       // this.cleanData(this.quadradoSrv.qtdItems);
        return data;
      })
  }

  /**
   * 
   * loadData
   * 
   * Recupera Resultado do metódo getJson().
   * 
   */
  loadData() {
    return this.getJSON();
  }

  cleanData(registros: number) {
    var d = [];
    for (var i = 0; i <= registros - 1; i++) {
      d.push(this.data[i])
    }
    this.data = d;
    if (this.data.length === 1) {
      console.log(this.data);
      this.goToApp(this.data[0])
    }
  }

  /**
   * 
   * sair
   * 
   * Exibe opções de sair do aplicativo.
   * 
   */
  sair() {
    this.showConfirm();
  }

    /**
   * showConfim
   * 
   * Mostra as opções para saída.
   */
  showConfirm() {
    this.confirma = this.alertController.create({
      title: 'Opções de saída',
      message: Config.mensagemGenerica.SAIR_CONFIRMAR,
      buttons: [
        {
          text: 'Sair',
          handler: () => {
            this.loginSrv.logout()
          }
        },
        {
          text: 'Encerrar',
          handler: () => {
            this.loginSrv.exitApp()
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            this.Tela_Saida_Visivel = false;
          }
        }
      ]
    });
    this.Tela_Saida_Visivel = true;
    this.confirma.present();
  }

}
