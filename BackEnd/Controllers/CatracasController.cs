﻿using Api.Mobile.PontoOnLine.utils;
using System.Web.Http;
using System.Web.Http.Cors;
//using System.Web.Mvc;
using Newtonsoft.Json;

namespace Api.Mobile.PontoOnLine.Controllers
{
    [RoutePrefix("api/catracas")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
  //  [EnableCors("AllowSpecificOrigin")]
    public class CatracasController : ApiController
    {
        catracasService catracasSrv = new catracasService();

        //[Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [HttpGet]
        [Route ("Get")]
        public string Get(int pagina = 0, int qtdPagina = 50)
        {
            return JsonConvert.SerializeObject(catracasSrv.GetCatracas(pagina, qtdPagina), Formatting.None);
        }
    }
}
