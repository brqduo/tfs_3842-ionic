
import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClient } from '@angular/common/http';
//import { RecebimentoPage } from '../Applications/Recebimento_Fisico/pages/recebimento/recebimento';
//import { RedirectPage } from '../pages/redirect/redirect';
import { HomePage } from '../pages/home/home';

//import { Observable } from 'rxjs/Observable';
import { LoginPage, ImageService, EnvironmentService, LoginService } from '@ons/ons-mobile-login';
import { CatracaPage } from '../Applications/Catraca_Online/pages/catraca/catraca';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  data: any;

  constructor(platform: Platform
    , statusBar: StatusBar
    , public http: HttpClient
    , public loginSrv: LoginService
    , public imageSrv: ImageService
    , public envSrv: EnvironmentService
    , splashScreen: SplashScreen
    , public app: App) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      imageSrv.SetDefault();
      loginSrv.setAplicationName('Mobile.MeuONS');
      envSrv.setEnv('TST');
      this.rootPage = LoginPage;

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

       this.loginSrv.onConectedChange
      .subscribe((u: any) => {
        if (u !== undefined) {
          if (u.Connected) {
            console.log('castle 2', u);
           this.app.getActiveNav().setRoot(HomePage);
          }
        }
      }) 
    });
  }


}
