import { Injectable } from '@angular/core';

/** CONFIG */
import { Config as config } from '../enviroment/config';


@Injectable()
export class QuadradoService {
  qtdItems: number = 2;
  AppsAllowed: number = 0;
  constructor() {

    // for (var x = 2; x < 16; x++) {
    //     for (var i = 1; i <= x; i++) {
    //         console.log(this.classeQuadradoPerfeito(i, x));
    //     }
    // }
  }

  /**
   * Retorna a distância do quadrado perfeito anterior mais próximo
   * @param num quantidade de icones na tela. Varia de 2 a 15
   */
  private distanciaQuadradoPerfeito(num): any {
    var result: any = {};
    result.distancia = -1;
    for (var i = 0; i <= config.quadradoPerfeito.length - 1; i++) {
      if (config.quadradoPerfeito[i].qp === num) {
        result = config.quadradoPerfeito[i];
        result.distancia = 0;
       // console.log('FINAL 1 RESULT', result);
      }
    }

    if (result.distancia === -1) {
      for (var x = 0; x <= config.quadradoPerfeito.length - 1; x++) {

        if (config.quadradoPerfeito[x].qp < num) {
          result = config.quadradoPerfeito[x];
          result.distancia = (num - config.quadradoPerfeito[x].qp);
       //   console.log('FINAL 2 RESULT', result);
          //this.config.quadradoPerfeito[x - 1].qp
        }
      }
    }
    return result;
  }

  /**
   * Retorna a classe a ser utilizada no item
   * @param posicao Posição do Item a ser renderizado
   * @param totalPagina Total de itens da página que esta sendo renderizada
   */
  public classeQuadradoPerfeito(posicao, totalPagina): string {
    if (this.rangeValid(this.qtdItems)) {
      if (this.LimitesQuadrados(posicao, totalPagina)) {
        var quadrado = this.distanciaQuadradoPerfeito(totalPagina);
    //    console.log('hell', quadrado);
        if (totalPagina === 2 || totalPagina === 0) {
    //      console.log('Acho q pode ser');
          return 'w-100';
        } else {
          if (posicao > quadrado.distancia) {
       //     console.log('posição maior', quadrado.distancia);
        //    console.log(quadrado.classes[0]);

            return quadrado.classes[0];
          } else {
        //    console.log('posição menor');

            return quadrado.classes[quadrado.distancia];
          }
        }
      } else {
        return 'w-100'
        //return 'mensagem de erro posicao ' + posicao + '  -  ' + totalPagina
      }
    } else {
      console.log('Erro na qtd de quadrados');

    }
  }

/**
 * 
 * rangeValid
 * 
 * Confere o tamanho do Quadrado e retorna bool conforme o críterio estabelecido
 * 
 * @param tamanho Tamanho do Quadrado
 */
  public rangeValid(tamanho): boolean {
    return ((tamanho >= 1) && (tamanho < 16))
  }

  /**
   * 
   * LimitesQuadrados
   * 
   * Retorna booleano conforme o atendimento dos critérios de limite de quadrados tela.
   * 
   * @param posicao Posicao do Quadrado
   * @param totalPagina Total de Quadrados da Pagina
   */
  private LimitesQuadrados(posicao: number, totalPagina: number): boolean {
    var retorno = (((posicao >= 1) && (posicao < 16)) && ((totalPagina > 1) && (totalPagina < 16)));
    // console.log(retorno);

    // if (retorno) {
    //     console.log(posicao, (posicao < totalPagina + 1));

    //     retorno = (posicao < totalPagina + 1)
    //     console.log(retorno);

    // }
    if (!retorno) {
      //   console.log('retorno false',posicao, totalPagina);
      //Gravar Log Error
    }
    return retorno;
  }

  /**
   * 
   * updateAppsAllowed
   * 
   * Atualiza o número de apps permitidos para o usuário.
   * 
   * @param totalPermitidos Número de Apps permitidos para o usuário
   */
  public updateAppsAllowed(totalPermitidos: number)
  {
    this.AppsAllowed = totalPermitidos == undefined ? 0 : totalPermitidos;
  }

}
