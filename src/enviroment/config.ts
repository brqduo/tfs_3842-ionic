export const Config = {
    production: false,
    quadradoPerfeito: [
        { qp: 2, classes: ['w-50', 'w-100', 'w-50'] },
        { qp: 4, classes: ['w-50', 'w-100', 'w-50'] },
        { qp: 6, classes: ['w-50', 'w-100', 'w-50'] },
        { qp: 9, classes: ['w-33', 'w-100', 'w-50'] },
        { qp: 12, classes: ['w-33', 'w-100', 'w-50'] },
        { qp: 15, classes: ['w-33', 'w-50', 'w-50'] }],
    mensagemGenerica: {
        ENTRANDO: 'Entrando...',
        SEM_APPS_PERMITDOS: 'Você não tem nenhum App autorizado para uso.',
        SAIR_CONFIRMAR_COM_VOLTAR: 'Você pode Voltar, Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.',
        SAIR_CONFIRMAR: 'Você pode Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.'
    },
    Error: {
        FINGER_ID_UNSUPORTED: 4400,
    }
};
