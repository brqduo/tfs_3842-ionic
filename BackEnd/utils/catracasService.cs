﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using Api.Mobile.PontoOnLine.model;
using Newtonsoft.Json;
using System.Web.Http;
using System.Web;
using RestSharp;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.IdentityModel.Claims;
using System.Configuration;

namespace Api.Mobile.PontoOnLine.utils
{
    public class catracasService
    {
        List<batidasModel> listaBatidas = new List<batidasModel>();
        Random random = new Random();

        public List<batidasModel> GetCatracas(int pagina = 0, int qtdPagina = 50)
        {
            var json = doRequests(qtdPagina, pagina);
          // var json = File.ReadAllText(@"c:\\temp\\Pontoonline.json");
            try
            {
                var y = ((Newtonsoft.Json.Linq.JContainer)(JsonConvert.DeserializeObject<Object>(json))).Last;

                int pos = 0;
                listaBatidas.Clear();
                DateTime dataBase = Convert.ToDateTime("1970-01-01");
                DateTime d = (DateTime)((Newtonsoft.Json.Linq.JContainer)y.First[0])["DataRegistroponto"];
                batidasModel b = new batidasModel();
                int inicio = ((pagina * qtdPagina) * 2) + 1;
                int limite = (inicio + (qtdPagina * 2)-1 );

                if (limite > y.First.Count() - 1)
                {
                    limite = (y.First.Count() - 1);
                }

                
                for (int i = ((pagina * qtdPagina) * 2); i <= limite; i++)
                {
                    d = (DateTime)((Newtonsoft.Json.Linq.JContainer)y.First[i])["DataRegistroponto"];
                    if ((dataBase.Year.ToString() + dataBase.Month.ToString() + dataBase.Day.ToString()) != (d.Year.ToString() + d.Month.ToString() + d.Day.ToString()))
                    {
                        pos = 0;
                        dataBase = (DateTime)((Newtonsoft.Json.Linq.JContainer)y.First[i])["DataRegistroponto"];
                    }

                    b.data = (DateTime)((Newtonsoft.Json.Linq.JContainer)y.First[i])["DataRegistroponto"];

                    if (pos % 2 == 0)
                    {
                        b.batida1 = ((Newtonsoft.Json.Linq.JContainer)y.First[i])["DataRegistroponto"].ToString();
                        pos++;
                    }
                    else
                    {
                        b.batida2 = ((Newtonsoft.Json.Linq.JContainer)y.First[i])["DataRegistroponto"].ToString();
                        listaBatidas.Add(b);
                        pos = 0;
                        b = new batidasModel();
                    }
                }
            }
            catch (Exception err)
            {
                batidasModel b = new batidasModel();
                b.idPontoOnline = -1;
                b.data = DateTime.Now;
                b.batida1 = "";
                b.batida2 = "";
                listaBatidas.Clear();
                listaBatidas.Add(b);
            }
            return listaBatidas.ToList();// response.Content;
        }
        private string doRequests(int qtdPagina, int pagina)
        {
            HttpWebResponse response;
            string responseText = "";

            if (getDataFromCatraca(out response))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        private static string ReadResponse(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                Stream streamToRead = responseStream;
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                {
                    streamToRead = new GZipStream(streamToRead, CompressionMode.Decompress);
                }
                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                {
                    streamToRead = new DeflateStream(streamToRead, CompressionMode.Decompress);
                }

                using (StreamReader streamReader = new StreamReader(streamToRead, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        private bool getDataFromCatraca(out HttpWebResponse response, int qtdPagina = 50, int Pagina = 0)
        {
            response = null;
            string tokenCatraca = "";
            string matricula = "";
            JwtSecurityToken token = getDecodeToken();
            tokenCatraca = getClaim(getSetting("urlTokenCatraca"), token).Value;
            matricula = getClaim(getSetting("propTokenMatricula"), token).Value;
            //a aqui eu preciso do claim d matricula que não existe no token que eu usei

            /*
             * Para testes enquanto não tratamos o problema do token estou utilizando o dado enviado como Cookie no header.
             * Caso este dado não exista eu assumo que não estmos mais com testes
             * 
             */

            tokenCatraca = getHeaderField("Cookie") == null ? tokenCatraca : getHeaderField("Cookie");
            matricula = getHeaderField("Matricula") == null ? matricula : getHeaderField("Matricula");

            string urlServico = getSetting("urlServicoCatraca");
            urlServico += string.Format("?$top={0}&$skip={1}&$filter=CodigoMatricula eq '{2}'&$orderby=DataRegistroponto desc", qtdPagina, (Pagina * qtdPagina), matricula);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["urlServicoCatraca"]);

                request.KeepAlive = true;
                request.Headers.Set(HttpRequestHeader.CacheControl, "max-age=0");
                request.Headers.Add("Upgrade-Insecure-Requests", @"1");
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
                request.Headers.Add("DNT", @"1");
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.Headers.Set(HttpRequestHeader.AcceptLanguage, "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6");
                request.Headers.Set(HttpRequestHeader.Cookie, ".ONSAUTH_VTPOP_01=" + tokenCatraca);
                //   request. .AddParameter("undefined", filtro, ParameterType.RequestBody);
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        JwtSecurityToken getDecodeToken()
        {
            HttpContext httpContext = HttpContext.Current;
            NameValueCollection headerList = httpContext.Request.Headers;
            var authorizationField = headerList.Get("Authorization").Replace("Bearer ","");
            var handler = new JwtSecurityTokenHandler();
            return handler.ReadToken(authorizationField) as JwtSecurityToken;
        }

        System.Security.Claims.Claim getClaim(string key, JwtSecurityToken token)
        {
            System.Security.Claims.Claim claim = null;
            foreach (System.Security.Claims.Claim c in token.Claims)
            {
                if (c.Type == key)
                {
                    claim = c;
                    break;
                }
            }
            return claim;
        }

        string getSetting(string key)
        {
            var appSettings = ConfigurationManager.AppSettings;
            return appSettings[key] ?? string.Empty;
        }

        string getHeaderField(string key)
        {
            HttpContext httpContext = HttpContext.Current;
            NameValueCollection headerList = httpContext.Request.Headers;
            return headerList.Get(key);
        }
    }
}
