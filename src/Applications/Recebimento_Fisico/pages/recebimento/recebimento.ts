import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage, AlertController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';
import { List } from 'ionic-angular';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
//import { RedirectPage } from '../../../../pages/redirect/redirect';
import { HomePage } from '../../../../pages/home/home';
import { LoginService } from '@ons/ons-mobile-login';
import { QuadradoService } from '../../../../services/quadraros.service';
import { Config } from '../../../../enviroment/config';

/**
 * RecebimentoPage
 */

@IonicPage()
@Component({
  selector: 'page-recebimento',
  templateUrl: 'recebimento.html'
})
export class RecebimentoPage {
  dadosObj: any[] = []
  itemExpandHeight: number = 480;
  confirma: any = {};
  @ViewChild(List) expandList: List;
  Tela_Saida_Visivel = false;
  totalAppsPermitidos: number = 0;

  graficoObj = {
    data: null as number[],
    type: null as string,
    options: null as Object,
    colors: null as Array<any>
  }

  omega = {
    'background-color': 'red'
  }

  progressCircleBar = {
    current: 10,
    max: 10,
    radius: 40,
    color: '#ffffff',
    background: 'transparent',
    stroke: 5,
    clockwise: false,
  }

  holdCardObj = {
    class: null as string,
    interval: null as any,
    text: null as string
  }
  showSearch: boolean = false


  constructor(
    public navCtrl: NavController, 
    private alertCtrl: AlertController,
    public loginSrv: LoginService,
    public QuadradosSrv: QuadradoService,
    public http: HttpClient) {
    //this.carregarDados();
    //this.expandList.sliding = true;
  }

  ionViewDidLoad() {
    this.carregarDados();
    this.totalAppsPermitidos = this.QuadradosSrv.AppsAllowed;
  }

  /**
   * 
   * holdCard
   * 
   * 
   * 
   * @param itemSelected Item selecionado
   * @param valor Valor do Item
   */

  holdCard(itemSelected, valor) {
    itemSelected.show = true
    console.log(valor);

    if (valor) {
      //  this.holdCardObj.class = 'cancel-aprovar'
      console.log(this.holdCardObj);

    }
    if (!valor) {
      //   this.holdCardObj.class = 'cancel-reprovar'
      console.log(this.holdCardObj);
    }
    this.expandList.closeSlidingItems()
    this.expandList.sliding = false;
    this.holdCardObj.interval = setInterval(() => {
      this.progressCircleBar.current = this.progressCircleBar.current - 1;
      if (this.progressCircleBar.current < -1) {
        this.progressCircleBar.current = 10;
        this.expandList.sliding = true;
        itemSelected.show = false
        itemSelected.backColorCard = null;
        itemSelected.LetterColor = null;
        clearInterval(this.holdCardObj.interval);
      }
    }, 1000);
  }

  /**
   * 
   * cancel
   * 
   * Cancela seleção do item.
   * 
   * @param itemSelected Item selecionado
   */
  cancel(itemSelected) {
    this.expandList.sliding = true;
    itemSelected.show = false;
    itemSelected.backColorCard = null;
    itemSelected.LetterColor = null;
    this.progressCircleBar.current = 10
    clearInterval(this.holdCardObj.interval);
  }

  /**
   * 
   * expandItem
   * 
   * Expande a caixa com o item selecionado.
   * 
   * @param item Item selecionado
   */
  expandItem(item) {

    this.dadosObj.map((listItem) => {
      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }
      return listItem;
    });
  }

  /**
   * 
   * carregarGrafico
   * 
   * Carrega gráfico acerca dos itens.
   *
   */
  carregarGrafico() {
    this.graficoObj.data = [75, 25];
    this.graficoObj.type = 'doughnut'
    this.graficoObj.options = {
      cutoutPercentage: 80
    }
    this.graficoObj.colors = [{
      backgroundColor: ['#F76C00',
        '#A6A6A6']
    }
    ]
  }


  /**
   * 
   * carregarDados
   * 
   * Carrrega os dados a serem exibidos nos itens.
   */
  carregarDados() {
    this.http.get("./assets/data/recebimento.json")
      .subscribe((dados: Array<any>) => {
        console.log("teste gulao");
        console.log(JSON.stringify(dados));
        // let v = []
        this.dadosObj = dados;
        for (let i = 0; i < this.dadosObj.length; i++) {
          this.dadosObj[i].show = false as Boolean;
          this.dadosObj[i].backColorCard = null as Object;
          this.dadosObj[i].LetterColor = null as Object;
        }
        this.carregarGrafico()

      })
  }

  /**
   * archive
   * 
   * 
   * 
   * @param ev 
   * @param path Caminho
   */
  archive(ev, path) {
    ev.expanded = false;
    ev.LetterColor = {
      'color': '#aba9a9'
    }
    if (path === 1) {
      /* #455E2B */
      ev.backColorCard = { 'background-color': '#60823d' }
      this.holdCardObj.text = 'Aprovação'
      console.log('break tru1', ev);
      this.holdCard(ev, true)
    }
    if (path === 0) {
      /* #D10429 */
      ev.backColorCard = { 'background-color': '#e88195' }
      this.holdCardObj.text = 'Reprovação'
      console.log('break tru2', ev);
      this.holdCard(ev, false)
    }

  }

  /**
   * Voltar()
   * 
   * Retorna a tela anterior.
   */

  voltar() {
    this.navCtrl.setRoot(HomePage)
  }

  /**
   * 
   * searchShow
   * 
   * Habikita a busca.
   * 
   * @param ev 
   */
  searchShow(ev) {
    if (ev === true) {
      this.showSearch = true
    }else{
      this.showSearch = false
    }
   /*  if (this.showSearch === false) {
      this.showSearch = true
    } else {
      this.showSearch = false
    } */
  }

   /**
   * 
   * sair
   * 
   * Exibe opções de sair do aplicativo.
   * 
   */
  sair() {
    if(this.totalAppsPermitidos > 1)
    {
    this.showConfirmComVoltar();
    }
    else
    {
      this.showConfirm();
    }
  }

    /**
   * showConfimComVoltar
   * 
   * Mostra as opções para saídam, incluindo o botão voltar.
   */
  showConfirmComVoltar() {
    this.confirma = this.alertCtrl.create({
      title: 'Opções de saída',
      message: Config.mensagemGenerica.SAIR_CONFIRMAR_COM_VOLTAR,
      buttons: [
        {
          text: 'Voltar',
          handler: () => {
            this.voltar();
          }
        },
        {
          text: 'Sair',
          handler: () => {
            this.loginSrv.logout()
          }
        },
        {
          text: 'Encerrar',
          handler: () => {
            this.loginSrv.exitApp()
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            this.Tela_Saida_Visivel = false;
          }
        }
      ]
    });
    this.Tela_Saida_Visivel = true;
    this.confirma.present();
  }

   /**
   * showConfim
   * 
   * Mostra as opções para saída.
   */
  showConfirm() {
    this.confirma = this.alertCtrl.create({
      title: 'Opções de saída',
      message: Config.mensagemGenerica.SAIR_CONFIRMAR,
      buttons: [
        {
          text: 'Sair',
          handler: () => {
            this.loginSrv.logout()
          }
        },
        {
          text: 'Encerrar',
          handler: () => {
            this.loginSrv.exitApp()
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            this.Tela_Saida_Visivel = false;
          }
        }
      ]
    });
    this.Tela_Saida_Visivel = true;
    this.confirma.present();
  }
}
