
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { UtilService } from './util.service';
import { VariaveisProd } from "./../config";
import moment from 'moment';

/**
 * 
 *   cA
 * 
 * 
 * 
 */

@Injectable()
export class CatracasService {
  objPontos = [];
  d1 = 1
  constructor(
    public utilSrv: UtilService,
    private httpClient: HttpClient,
  ) {
    moment.locale('pt-br')
  }
  /**
   * getData
   * 
   * Recupera marcações de ponto.
   * 
   * Data ínicio para recuperar os pontos até o dia atual
   * @param dtUltima
   * 
   */
  getData(dtUltima) {
    const httpOptions = { headers: new HttpHeaders({ "responseType": 'json' }) };
    // Get?pagina=1&qtdPagina=5
    return this.httpClient.get(VariaveisProd.catraca_url 
    + '/Get?pagina=' + dtUltima.pagina 
    + '&qtdPagina=' + dtUltima.qtdPagina, httpOptions)
      .map(data => {      
        return data;
      });
  }
  /**
   * getRandomInt
   * 
   * retorna um número aletaório.
   * 
   * @param min Valor mínimo
   * @param max Valor máximo
   */
  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}