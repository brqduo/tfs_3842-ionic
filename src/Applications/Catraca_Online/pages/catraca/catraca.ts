import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, } from 'ionic-angular';
import moment from 'moment';
import { Observable } from 'rxjs';
import { UtilService } from '../../services/util.service';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { LoginService } from '@ons/ons-mobile-login';
import { CatracasService } from '../../services/catracas.service';
@Component({
  selector: 'page-catraca',
  templateUrl: 'catraca.html'
})

export class CatracaPage {

  listaDias = [];
  listaBatidas = [];
  temBatidas = [];
  show: boolean;
  Tela_Saida_Visivel: boolean;
  urlData = { pagina: 0, qtdPagina: 10 }
  batidasArray = []

  xcont = 0
  testeBind: string = '<ul class="baloon"><li class="hours">  <div class="arrival">{{d.entrada}}</div>  <div class="exit">{{d.saida}}</div></li></ul>'

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public catracaSrv: CatracasService,
    public loginSrv: LoginService,
    public AnalyticsSrv: AnalyticsService,
    public utilSrv: UtilService,
    public alertController: AlertController) {
    this.doRefresh()
    moment.locale('pt-br')

  }
  ionViewDidLoad() {
    this.getData(this.urlData)
  }

  sair() {
    console.log("<=|=>");
    this.showConfirm()
  }

  showConfirm() {
    let confirma = this.alertController.create({
      title: 'Deseja mesmo sair?',
      message: 'Você tem certeza que deseja mesmo se deslogar do aplicativo?',
      buttons: [
        {
          text: 'Sair',
          handler: () => {
            this.Tela_Saida_Visivel = false;
            this.loginSrv.logout();
            this.AnalyticsSrv.sendCustomEvent("Sair");
            /* this.navCtrl.setRoot(LoginPage); */
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            this.Tela_Saida_Visivel = false;
          }
        }
      ]
    });
    this.Tela_Saida_Visivel = true;
    confirma.present();
  }

  unique(a: any) {
    var retorno: any = []
    a.sort();
    for (let i = 0; i < a.length; i++) {
      var element = a[i];
      const temp_dt: any = new Date(element.dataDia);
      const dt = new Date(temp_dt.getFullYear(), temp_dt.getMonth(), temp_dt.getDate());
      var dia: any = dt.getDate();
      var mes: any = dt.getMonth()
      mes = moment.monthsShort(mes)
      let achou = false;
      for (var x = 0; x < retorno.length; x++) {
        if ((retorno[x].data.getTime() == temp_dt.getTime())) {
          achou = true
        }
      }
      if (!achou) {
        retorno.push({ data: dt, dia: dia, mes: mes })
      }
    }
    return retorno
  }

  getData(dados): Observable<any> {
    return this.catracaSrv.getData(dados)
      .map((data: string) => {
        JSON.parse(data).forEach(element => {
          const temp_dt: any = new Date(element.data);
          const dt = new Date(temp_dt.getFullYear(), temp_dt.getMonth(), temp_dt.getDate());
          element.dataDia = dt;
          this.temBatidas.push(element)
        })
        this.listaDias = this.unique(this.temBatidas);
      })
  }


  doRefresh(refresher?) {
    if (refresher === undefined) {
      console.log('123456');
      this.listaDias = [];
      this.temBatidas = []
      this.urlData.pagina = 0;
      this.urlData.qtdPagina = 10;
      this.getData(this.urlData).subscribe((s) => {
      })
    } else {
      console.log('Função pullToRefresh=> \n', refresher);
      this.listaDias = [];
      this.temBatidas = []
      this.urlData.pagina = 0;
      this.urlData.qtdPagina = 10;
      this.getData(this.urlData).subscribe((s) => {
        refresher.complete();
      })

    }

  }

  getlistaBatidas(item) {
    var res = '<ul class="baloon">'
    var existembatidas = false;
    this.temBatidas.forEach(element => {
      const dia: any = new Date(element.data).getDate();
      const mes: any = moment.monthsShort(new Date(element.data).getMonth())
      const entrada = element.batida1.substr(11, 5)
      const saida = element.batida2.substr(11, 5)
      const obj = { data: element.data, batida1: entrada, batida2: saida }
      if (entrada === "Invalid date") {
        //console.log('ObjetoInvalido',obj);

        // console.log('Elemento 5',element.batida1)
        // console.log('ENTRADA',moment(new Date(element.data)).format());
        //  console.log('ENTRADA ERRADA',moment(new Date(element.batida1.Trim())) .format('LT'));
        //console.log('SAIDA',moment(new Date(element.batida2)).format());
        // console.log('entrada',entrada);
        // console.log('saida',saida);
      } else {
        //console.log('ObjetoLimpo',obj);
        //  console.log('Elemento 6',element)
        //  console.log('ENTRADA CERTA',new Date(element.batida1))
        // console.log('saida',saida);

      }
      if ((dia == item.dia) && (mes == item.mes)) {
        res += '<li class="hours">'
        res += '<div class="arrival">' + obj.batida1 + '</div>'
        res += '<div class="exit">' + obj.batida2 + '</div>'
        res += '</li>';
        existembatidas = true;
      }
    });
    if (!existembatidas) {
      res = '';
    }
    res = res + '</ul>'
    return res
  }


  doInfinite(InfiniteScroll) {
    this.xcont++;
    console.log('XCONT', this.xcont);
    this.urlData.pagina++
    this.getData(this.urlData)
      .subscribe(data => {
        InfiniteScroll.complete()
      })
  }
}
