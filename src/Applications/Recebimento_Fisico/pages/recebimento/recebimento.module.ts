import { RecebimentoPage } from './recebimento';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpandableComponent } from '../../../../components/expandable/expandable';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { ChartsModule } from 'ng2-charts';

@NgModule({
    declarations:[
        RecebimentoPage,
        ExpandableComponent,
    ],
    imports:[
        ChartsModule,
        RoundProgressModule,
        IonicPageModule.forChild(RecebimentoPage),
    ],
    entryComponents: [
        ExpandableComponent,
    ],
    exports: [
        RecebimentoPage
    ]
})

export class RecebimentoPageModule{ }
