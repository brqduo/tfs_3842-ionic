// ----- Desenvolvimento
// export const SERVER_URL = "http://popdsv.ons.org.br/ons.pop.federation/oauth2";
//export const INTRANET_URL = "http://localhost/ramais/api";
// rede fisica export const INTRANET_URL = "http://10.2.101.157/contatoSrv/contatos/";
// export const INTRANET_URL = "https://appapitst.ons.org.br/api/contatos/";
//export const INTRANET_URL = "http://10.21.6.73/api/contatoSrv/contatos/";
//export const INTRANET_URL = "http://localhost:51078/contatos/";
// export const INTRANET_URL = "http://rj-dk-273-22637.ons.org.br/services.corp.dados.ListaRamaisAPI/api";

// ----- Testes

// export const SERVER_URL = "http://popdsv.ons.org.br/ons.pop.federation/oauth2";
export const VariaveisProd = {
    SERVER_URL: "https://appapitst.ons.org.br/ons.pop.federation/oauth2",
    INTRANET_URL: "http://localhost:64631/",
    REFRESH_INTERVAL: 24 * 60 * 1000, // CALCULO EM MINUTOS
    CLIENT_ID: "Mobile.Contatos",// "Mobile.MeuONS";
    GRANT_TYPE: "password",
    WHITELISTEDDOMAINS: ["ons.org.br", "http://10.2.101.157:8100", "http://10.2.101.157", "*"],
    teste: 'nome de teste',
    /* catraca_url: 'http://srv17520.brq.com/ons.ponto.online/api/catracas' */
    /* catraca_url:'http://10.2.101.157/onsPontoOnline/api/catracas' */
    /* catraca_url: 'http://pontoonline252.azurewebsites.net/api/catracas' */
    catraca_url: 'http://10.2.1.127/ponto/api/catracas'
}

export const SERVER_URL = "https://appapitst.ons.org.br/ons.pop.federation/oauth2";
export const INTRANET_URL = "http://localhost:64631/";
// export const INTRANET_URL = "http://localhost:51078/api/contatos/";
export const REFRESH_INTERVAL = 24 * 60 * 1000; // CALCULO EM MINUTOS
export const CLIENT_ID = "Mobile.Contatos";// "Mobile.MeuONS";
export const GRANT_TYPE = "password";
export const WHITELISTEDDOMAINS: string[] = ["ons.org.br", "http://10.2.101.157:8100", "http://10.2.101.157", "*"];
export const Config = {
    teste: 'nome de teste',
    /* catraca_url: 'http://srv17520.brq.com/ons.ponto.online/api/catracas' */
    /* catraca_url:'http://10.2.101.157/onsPontoOnline/api/catracas' */
    /* catraca_url: 'http://pontoonline252.azurewebsites.net/api/catracas' */
    catraca_url: 'http://10.2.1.127/ponto/api/catracas'
}