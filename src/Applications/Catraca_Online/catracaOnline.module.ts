
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
//Paginas
import { CatracaPage } from './pages/catraca/catraca';
//Serviços
import { UtilService } from './services/util.service';
import { CatracasService } from './services/catracas.service';

const commonImports = [CommonModule, IonicModule, FormsModule, ReactiveFormsModule];

const page = [ CatracaPage ]

const service = [ UtilService, CatracasService ]

@NgModule({
  declarations: [],
  imports: [ ...commonImports ],
  exports: [ ...commonImports ],
  entryComponents: []
  
})
export class catracaModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: catracaModule,
      providers: [...service],
    };
  }
 }
