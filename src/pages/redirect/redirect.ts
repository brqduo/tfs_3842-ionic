import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QuadradoService } from '../../services/quadraros.service'
import { HttpClient } from '@angular/common/http';
// import { RecebimentoPage } from '../../Applications/Recebimento_Fisico/pages/recebimento/recebimento';

@Component({
  selector: 'page-redirect',
  templateUrl: 'redirect.html'
})
export class RedirectPage {
  directs: any
  qtdQuadrados: number = 7
  options = [
    {
      nome: 'Pagina Principal',
      page: HomePage
    }, 
    // {
    //   nome: 'Pagina Recebimento',
    //   page: RecebimentoPage
    // }
  ]
  constructor(public navCtrl: NavController
    , public quadradoSrv: QuadradoService
    , public http: HttpClient) {

  }


  goToApp() {
    //this.navCtrl.push(HomePage)
    this.quadradoSrv.qtdItems = this.qtdQuadrados;
    if (this.directs !== undefined) {
      this.navCtrl.push(this.directs)
    }

  }


}
