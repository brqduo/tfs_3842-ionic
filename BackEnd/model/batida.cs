﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Mobile.PontoOnLine.model
{
    public class batidasModel
    {
        public DateTime data { get; set; }
        public string batida1 { get; set; }
        public string batida2 { get; set; }
        public int idPontoOnline { get; set; }
    }
}
