import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HttpClientModule } from '@angular/common/http';

/** SERVICES */
import { QuadradoService } from './../services/quadraros.service';
import { LoginPage, 
  LoginService, 
  UtilService, 
  TokenService, 
  ErrorService, 
  StorageService, 
  ImageService, 
  SecurityService, 
  NetWorkService, 
  EnvironmentService,
  UserService} from '@ons/ons-mobile-login';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
//import { catracaModule } from '../Applications/Catraca_Online/src/catracaOnline.module';
import { catracaModule } from '../Applications/Catraca_Online/catracaOnline.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
     mode: 'md',
      backButtonIcon:'ios-arrow-back',
      backButtonText: ''
        }
    ),
    catracaModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    QuadradoService,
    SplashScreen,
    LoginService,
    UtilService,
    AnalyticsService,
    TokenService,
    ErrorService,
    StorageService,
    ImageService,
    SecurityService,
    NetWorkService,
    EnvironmentService,
    UserService,
    FingerprintAIO,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
