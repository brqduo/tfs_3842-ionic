import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { Observable, Subject } from 'rxjs';
import "rxjs/add/observable/of";
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular'
import { AlertController } from 'ionic-angular';

import { Storage } from "@ionic/storage";
import { Platform, LoadingController, Loading } from 'ionic-angular';

//import { AuthProvider } from '../providers/auth/auth';

@Injectable()
export class UtilService {

    posMsg: any = ['bottom', 'top', 'middle'];
    filtroAtivo: string = '';
    EmAnaliseErro = false;

    config: any = {};
    loading: Loading;

    refreshContatos = new Subject();
    ObservableFecharGerencias = new Subject<any>();



    constructor(public toastSrv: ToastController,
        public alertCtrl: AlertController,
        private loadingController: LoadingController,
        public platform: Platform,
        public storage: Storage,
        private analyticSrv: AnalyticsService,
            ) {
        this.config.banco = {};
        this.config.banco.url = 'keychain.br.org.ons';
        this.config.textKeys = {};
        this.config.textKeys.chaveLogin = 'automaticLogin'
        this.config.textKeys.jwtTokenName = 'jwt_token'
        this.config.textKeys.ForceLogin = 'ForceLogin';
    }

    alerta(mensagem, delay?: number, pos?: string, titulo?: string, btnClose?: boolean) {
        delay = (((delay === undefined) || (delay === null)) ? 3000 : delay);
        pos = ((this.posMsg.indexOf(pos) === -1) ? 'middle' : pos);
        btnClose = (((btnClose === undefined) || (btnClose === null)) ? false : btnClose);
        mensagem = (((titulo == undefined) || (titulo == null)) ? mensagem : titulo + '\n\n' + mensagem);
        const toast = this.toastSrv.create({ message: mensagem, duration: delay, position: pos, showCloseButton: btnClose, closeButtonText: 'Fechar' })
        toast.present();
    }


    ErrorAlerta(mensagem, delay?: number, pos?: string, titulo?: string, btnClose?: boolean) {
        if (this.EmAnaliseErro) {
            delay = (((delay === undefined) || (delay === null)) ? 3000 : delay);
            pos = ((this.posMsg.indexOf(pos) === -1) ? 'middle' : pos);
            btnClose = (((btnClose === undefined) || (btnClose === null)) ? false : btnClose);
            mensagem = (((titulo == undefined) || (titulo == null)) ? mensagem : titulo + '\n\n' + mensagem);
            const toast = this.toastSrv.create({ message: mensagem, duration: delay, position: pos, showCloseButton: btnClose, closeButtonText: 'Fechar' })
            toast.present();
        }
    }

    ativarBloqueioMsg(msg?: string) {
        const mensagem = (((msg == undefined) || (msg == null)) ? 'logando...' : msg);
        this.loading = this.loadingController.create({
            content: mensagem
        });
        this.loading.present();
    }

    desativarBloqueioMsg() {
        this.loading.dismiss();
    }

    //#region Rotina de gravação do banco
    StorageInsertInto(key: string, val?: any): Observable<any> {
        return Observable.fromPromise(
            this.storage.set(key, val)
                .then(value => {
                    return (value)
                })
                .catch(error => {
                    this.analyticSrv.sendNonFatalCrash("storage.set(automaticLogin)", error);
                    return (error)
                })
        );
    }

    StorageRemoveFrom(key: string) {
        /**
         * Esta dando erro de undefined no retorno
         * Como não é necessário neste momento estou deixando para depois
         * Preciso confirmar  se a gravação esta ok.
         */
        return Observable.fromPromise(
            this.storage.remove(key)
                .then(value => {
                    return (value)
                })
                .catch(error => {
                    this.analyticSrv.sendNonFatalCrash("storage.set(automaticLogin)", error);
                    return (error)
                })
        );
    }

    StorageGetFromByKey(key: string) {
        return Observable.fromPromise(
            this.storage.get(key)
                .then(value => {
                    return (value)
                })
                .catch(error => {
                    this.analyticSrv.sendNonFatalCrash("storage.set(automaticLogin)", error);
                    return (error)
                })
            //console.log('passou batido')
        );
    }

    StorageInitialize() {

    }
    //#endregion


    //#region Rotinas de encerramento do app

    /**
     * Faz o encerramento da aplicação.
     * Somente nãoé exigida a confirmação se o desenvolvedor for explícito na chamada
     * Por padrão o parametro dera true caso o usuário não informe
     * 
     * @param {boolean} [confirm] 
     * 
     * @memberOf UtilService
     */
    exitApp(confirm?: boolean) {
        confirm = ((confirm === undefined || confirm == null) ? true : confirm)
        if (confirm) {
            this.alertCtrl.create({
                title: 'Deseja mesmo sair?',
                message: 'Você tem certeza que deseja mesmo se deslogar do aplicativo?',
                buttons: [
                    {
                        text: 'Não'
                    },
                    {
                        text: 'Sim',
                        handler: () => {
                            this.analyticSrv.sendCustomEvent("Sair");
                            this.platform.exitApp();
                        }
                    }
                ]
            });
        }
        else {
            this.analyticSrv.sendCustomEvent("Sair Sem confirmação");
            this.platform.exitApp();
        }
    }

    padLeft(text: string, padChar: string, size: number): string {
        return (String(padChar).repeat(size) + text).substr((size * -1), size);
    }

    getFormData(object) {
        const formData = new FormData();
        Object.keys(object).forEach(key => formData.append(key, object[key]));
        return formData;
    }

    //#endregion

    ForceLoginGet() {
        return this.StorageGetFromByKey(this.config.textKeys.ForceLogin)
            .map(data => {
                console.log('data from force login GET ', data)
                if (data === null) {
                    data = false
                }
                return data
            })
    }

    ForceLoginSetTrue() {
        return this.StorageInsertInto(this.config.textKeys.ForceLogin, true)
            .map(data => {
                console.log('data from force login ', data)
                return data
            })
    }

    ForceLoginSetFalse(): Observable<any> {
        return this.StorageInsertInto(this.config.textKeys.ForceLogin, false)
            .map(data => {
                console.log('data from force login false ', data)
                return data
            })
    }
    //#endregion


}